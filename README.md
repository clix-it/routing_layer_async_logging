# Delphi 2.0 #

This is the async database logging layer.

## Routing Layer Async Logging ##

### This layer would do the following activities ###

* Log JSON Request/Response in the Database.

### The following key-values ​​are stored in environment variables.

#### UAT:
| Key  | Value |
| ------------- | ------------- |
| GET_PASSWORD_ARN | arn:aws:lambda:ap-south-1:443390217582:function:getPassword |
| database | routing_layer |
| host | uat-laas-postgres.clix.capital |
| is_sentry | True |
| port | 5432 |
| user | delphi2_uat |

#### PROD:
| Key              | Value |
|------------------| ------------- |
| GET_PASSWORD_ARN | arn:aws:lambda:ap-south-1:371696937501:function:getDbDettails |
| database         | routing_layer |
| host             | laas-prod-postgres.clix.capital |
| env              | Production |
| port             | 5432 |
| user             | delphi2_prod |

### How do I get set up? ###

* [AWS Login](https://clix.awsapps.com/start#/)
* [Documents](https://clixit.atlassian.net/wiki/spaces/DELP/overview?homepageId=822575325)

### Lambda Details

* Layers: PYTHON_PACKAGES:1
* RunTime: Python3.7
* Function Name: routing_layer_async_logging

### Who do I talk to? ###
* Repo owner or team contact

