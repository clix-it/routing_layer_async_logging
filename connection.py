import psycopg2
import json
import os
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Database:
    def __init__(self, event=None, password=None):
        self.data = event
        self.password = password
        self.conn = None
        self.warning = []
        self.cursor = self.database_connection()
        self.update_db = self.update_database()

    def database_connection(self):
        try:
            # connect to the PostgreSQL server
            self.conn = psycopg2.connect(host=os.environ['host'], port=os.environ['port'],
                                         database=os.environ['database'], user=os.environ['user'],
                                         password=self.password)
            # create a cursor
            self.cursor = self.conn.cursor()
            return self.cursor
        except psycopg2.DatabaseError as e:
            logger.error(e)
            return logger.error(e)

    def update_database(self):
        try:
            request_json = self.data["Request"]

            try:
                stage = request_json["report"]["stage"]["applicationStage"]
            except:
                stage = None

            response_json = json.loads(self.data["Response"])

            try:
                resp_json = response_json["bureau_response"]
            except:
                resp_json = response_json["response"]

            print("applicationId", request_json["applicationId"])

            try:
                mobile = resp_json["calculated_variables"]["mobile_number"]
            except:
                mobile = None

            scorecard = response_json['scorecard']['scorecard_name']
            version = response_json['scorecard']['scorecard_version']

            source = f"{scorecard}:{version}"
            print('source ', source)
            scorecard_ref = self.data["scorecard_ref"]

            is_ab_testing = bool(self.data["is_ab_testing"])

            requestId = self.data["requestId"]

            status = resp_json["decision"]["status"]

            try:
                app_stage = resp_json["extra_details"]["applicationStage"]
            except:
                app_stage = None

            if app_stage in ["personal_info", "professional_info"]:
                loan_amount = 0
                roi = 0
                tenure = 0
            else:
                try:
                    loan_amount = int(resp_json["eligibility"][0]["loanAmount"])
                    roi = float(resp_json["eligibility"][0]["roi"])
                    tenure = int(resp_json["eligibility"][0]["tenure"])
                except:
                    loan_amount = 0
                    roi = 0
                    tenure = 0

            postgres_insert_query = "INSERT INTO req_resp_scorecards (request_id, source, scorecard_ref, is_ab_applicable, product, partner, loan_app_id, customer_id, stage, request_json, response_json, status, approved_loan_amt, approved_roi, approved_tenure) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

            record_to_insert = (
            requestId, source, scorecard_ref, is_ab_testing, request_json["product"], request_json["partner"],
            request_json["applicationId"], request_json["customerId"], stage, json.dumps(request_json),
            json.dumps(response_json), status, loan_amount, roi, tenure)
            print('postgres_insert_query ', postgres_insert_query)
            print('record_to_insert ', record_to_insert)
            self.cursor.execute(postgres_insert_query, record_to_insert)
            self.conn.commit()
            msg = "Record inserted successfully"
            print(msg)
            self.warning.append(msg)

        except (Exception, psycopg2.Error) as error:
            if (self.conn):
                msg = "Failed to insert record into table"
                print(msg)
                print(error)
                self.warning.append(msg)

        finally:
            # closing database connection.
            if (self.conn):
                self.cursor.close()
                self.conn.close()
                msg = "PostgreSQL connection is closed"
                print(msg)
                self.warning.append(msg)

        return self.warning
