import json
import logging
import boto3
from base64 import b64decode
import zlib, base64
from connection import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)

client = boto3.client('lambda')
presponse = client.invoke(
    FunctionName=os.environ['GET_PASSWORD_ARN'],
    InvocationType='RequestResponse',
    Payload=json.dumps({"queryStringParameters": {"secret_name": "delphi2_psql"}})
)
precords = json.load(presponse['Payload'])
password = dict(precords).get('body').get('password')


def lambda_handler(event, context):
    if "encoded_req_resp_json" in event:
        logger.info("Encoded Json Logging")
        try:
            j = zlib.decompress(base64.b64decode(event["encoded_req_resp_json"]))
        except:
            raise RuntimeError("Could not decode/unzip the contents")

        try:
            event = json.loads(j)
        except:
            raise RuntimeError("Json load error at runtime")

    db = Database(event, password)
    resp = db.update_db

    req_id = context.aws_request_id

    request_id = event["requestId"]

    logger.info("---------Request Id---------")
    logger.info(request_id)
    logger.info("---------Log Response---------")
    logger.info(resp)

    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": {"Response": resp, "AwsRequestId": req_id, "DelphiRequestId": request_id},
        "isBase64Encoded": False
    }
